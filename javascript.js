var myAddButton = document.getElementById("my-button");
var myList = document.getElementById("list");

var myButtonFunction = function () {
	var myTask = document.getElementById("task").value;
	if(myTask.length > 0){
		var liTag = document.createElement("li");
		liTag.innerHTML = myTask;
		myList.appendChild(liTag);
		var myDeleteButton = document.createElement("button");
		myDeleteButton.setAttribute("class", "delete");
		myDeleteButton.innerHTML = "X";
		myDeleteButton.addEventListener("click", myDeleteFunction);
		liTag.appendChild(myDeleteButton);
		document.getElementById("task").value = "";
	}
};

myAddButton.addEventListener("click", myButtonFunction);

var myDeleteFunction = function (event) {
	event.target.parentElement.parentNode.removeChild(event.target.parentElement);
};